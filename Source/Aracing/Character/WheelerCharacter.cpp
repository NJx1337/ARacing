// Fill out your copyright notice in the Description page of Project Settings.


#include "WheelerCharacter.h"
#include "GameFramework/SpringArmComponent.h"

AWheelerCharacter::AWheelerCharacter()
{
	MeshWheelchair = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("MeshWheelchair"));
	MeshWheelchair->SetupAttachment(GetMesh());

	//GetMesh()->bDisableMorphTarget = true;
	GetMesh()->SetRelativeLocation(FVector(0, 0, -89.0f));
	GetMesh()->SetRelativeRotation(FRotator(0, -90.f, 0));

	MeshShirt->SetRelativeScale3D(FVector(1.01f, 1.02f, 1.002f));

	MeshShorts->SetRelativeScale3D(FVector(1.03f, 1.f, 1.05f));
	MeshShorts->SetRelativeLocation(FVector(0, 0, -2.75f));

	// Disable bUseControllerRotationYaw, because we turn the wheeler character over MoveRight, not the camera
	bUseControllerRotationYaw = false;

	AccelerationDuration = 0.2f;
	bAccelerationReady = true;
	bBreakActive = false;
	CurrentAcceleration = 0.f;
}

void AWheelerCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	// Acceleration boost
	if (CurrentAcceleration != 0.f)
	{
		AddMovementInput(GetActorForwardVector() * CurrentAcceleration);
	}
}

void AWheelerCharacter::MoveForward(float Value)
{
	if (bAccelerationReady && Value != 0.f)
	{
		CurrentAcceleration = Value > 0.f ? 1.f : -1.f;
		bAccelerationReady = false;
		GetWorldTimerManager().SetTimer(AccelerationDurationTimer, this, &AWheelerCharacter::AccelerationDurationTimerFinished, AccelerationDuration);
	}
}

void AWheelerCharacter::MoveRight(float Value)
{
	// Overwrite MoveRight to a Turn type move. Wheeler can not move sidewards.

	float RotateAmount = Value * RotateSpeed * GetWorld()->DeltaTimeSeconds;
	FRotator Rotation = FRotator(0, RotateAmount, 0);
	RotationDirection = FQuat(Rotation);

	AddActorLocalRotation(RotationDirection, true);
	AddControllerYawInput(RotateAmount / 2.5f);
}

void AWheelerCharacter::AccelerationDurationTimerFinished()
{
	CurrentAcceleration = 0.f;
}

void AWheelerCharacter::AccelerateStart()
{
	FDateTime timeUtc = FDateTime::UtcNow();
	AccelerateMeassureTime = timeUtc.ToUnixTimestamp() * 1000 + timeUtc.GetMillisecond();
}

void AWheelerCharacter::AccelerateFinish()
{
	FDateTime timeUtc = FDateTime::UtcNow();
	AccelerateMeassureTime = timeUtc.ToUnixTimestamp() * 1000 + timeUtc.GetMillisecond() - AccelerateMeassureTime;
	//UE_LOG(LogTemp, Warning, TEXT("TIME Anim-Execution time: %f"), AccelerateMeassureTime / 1000.f);
	AccelerateMeassureTime = 0.f;
}

void AWheelerCharacter::AccelerateReady()
{
	bAccelerationReady = true;
}
