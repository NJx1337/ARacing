// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Animation/AnimInstance.h"
#include "BaseCharAnimInstance.generated.h"

/**
 * 
 */
UCLASS()
class ARACING_API UBaseCharAnimInstance : public UAnimInstance
{
	GENERATED_BODY()

public:
	UBaseCharAnimInstance();

	// Updating animation properties
	virtual void NativeUpdateAnimation(float DeltaSeconds) override;
	
};
