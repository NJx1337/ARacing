// Fill out your copyright notice in the Description page of Project Settings.


#include "WheelerCharAnimInstance.h"
#include "WheelerCharacter.h"
#include "Kismet/KismetMathLibrary.h"

UWheelerCharAnimInstance::UWheelerCharAnimInstance()
{
	MovementState = EWheelerMovementState::EWMS_Standing;
	LeanDelta = 0.f;
	CharacterRotation = FRotator(0.f);
	CharacterRotationLastFrame = FRotator(0.f);
}

void UWheelerCharAnimInstance::NativeUpdateAnimation(float DeltaSeconds)
{
	Super::NativeUpdateAnimation(DeltaSeconds);

	if (WheelerCharacter == nullptr)
	{
		WheelerCharacter = Cast<AWheelerCharacter>(TryGetPawnOwner());
	}
	else
	{
		CharacterRotationLastFrame = CharacterRotation;
		CharacterRotation = WheelerCharacter->GetActorRotation();

		const FRotator Delta = UKismetMathLibrary::NormalizedDeltaRotator(CharacterRotation, CharacterRotationLastFrame);
		const float Target = Delta.Yaw / DeltaSeconds;
		const float InterpedValue = FMath::FInterpTo(LeanDelta, Target, DeltaSeconds, 6.f);

		LeanDelta = FMath::Clamp(InterpedValue, -90.f, 90.f);

		if (WheelerCharacter->GetCurrentAcceleration() != 0)
		{
			MovementState = EWheelerMovementState::EWMS_Accelerating;
		}
		else
		{
			if (WheelerCharacter->GetAccelerationReady())
			{
				MovementState = EWheelerMovementState::EWMS_Standing; // Acceleration Animation finished
			}
			else
			{
				MovementState = EWheelerMovementState::EWMS_Rolling; // Still in Acceleration Animation
			}
		}

		//if (GEngine)
		//{
		//	GEngine->AddOnScreenDebugMessage(2, -1, FColor::Green, FString::Printf(TEXT("LeanDelta: %f"), LeanDelta));
		//	GEngine->AddOnScreenDebugMessage(3, -1, FColor::White, FString::Printf(TEXT("CharacterRotation: %s"), //*CharacterRotation.ToString()));
		//	GEngine->AddOnScreenDebugMessage(4, -1, FColor::White, FString::Printf(TEXT("CharacterRotationLastFrame: %s"), //*CharacterRotationLastFrame.ToString()));
		//}
	}
}

void UWheelerCharAnimInstance::NativeInitializeAnimation()
{
	WheelerCharacter = Cast<AWheelerCharacter>(TryGetPawnOwner());
}
