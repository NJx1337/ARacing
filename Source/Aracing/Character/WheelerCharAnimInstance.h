// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BaseCharAnimInstance.h"
#include "WheelerCharAnimInstance.generated.h"

UENUM(BlueprintType)
enum class EWheelerMovementState : uint8
{
	EWMS_Standing UMETA(DisplayName = "Standing"),
	EWMS_Accelerating UMETA(DisplayName = "Accelerating"),
	EWMS_Rolling UMETA(DisplayName = "Rolling"),
	EWMS_Breaking UMETA(DisplayName = "Breaking"),

	EWMS_MAX UMETA(DisplayName = "DefaultMax")
};

/**
 * 
 */
UCLASS()
class ARACING_API UWheelerCharAnimInstance : public UBaseCharAnimInstance
{
	GENERATED_BODY()

public:
	UWheelerCharAnimInstance();

	// Updating animation properties
	virtual void NativeUpdateAnimation(float DeltaSeconds) override;

	virtual void NativeInitializeAnimation() override;

protected:
	//UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Movement, meta = (AllowPrivateAccess = "true"))
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	class AWheelerCharacter* WheelerCharacter;

	/** State that describes the current movement situation for animation **/
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Movement")
	EWheelerMovementState MovementState;

	/** Yaw delta used for leaning in the forward blendspace **/
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	float LeanDelta;

private:
	/** Character Rotation this frame **/
	FRotator CharacterRotation;

	/** Character Rotation last frame **/
	FRotator CharacterRotationLastFrame;
	
};
