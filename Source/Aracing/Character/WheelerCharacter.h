// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BaseCharacter.h"
#include "WheelerCharacter.generated.h"

/**
 * 
 */
UCLASS()
class ARACING_API AWheelerCharacter : public ABaseCharacter
{
	GENERATED_BODY()

public:
	AWheelerCharacter();

	// Called every frame
	virtual void Tick(float DeltaTime) override;

protected:
	virtual void MoveForward(float Value) override;

	virtual void MoveRight(float Value) override;

	/** Skeletal mesh for wheelchair. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Character)
	USkeletalMeshComponent* MeshWheelchair;

	/** Timer that will stops the acceleration after defined time **/
	FTimerHandle AccelerationDurationTimer;

	/** The time the acceleration will hold on **/
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Movement)
	float AccelerationDuration;

	/** True if next acceleration is ready **/
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Movement)
	bool bAccelerationReady;

	/** True if there is movement and it will be deaccelerated by breaking **/
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Movement)
	bool bBreakActive;

	/** Acceleration value/direction **/
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Movement)
	float CurrentAcceleration;

	/** Base Speed how fast the character can rotate under optimal circumstances **/
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Movement")
	float RotateSpeed = 100.f;

	/** Will get called when AccelerationDurationTimer is finished **/
	UFUNCTION()
	void AccelerationDurationTimerFinished();

	int64 AccelerateMeassureTime = 0;

private:
	UFUNCTION(BlueprintCallable)
	void AccelerateStart();

	UFUNCTION(BlueprintCallable)
	void AccelerateFinish();

	UFUNCTION(BlueprintCallable)
	void AccelerateReady();

	// The used FQuat to rotate the character
	FQuat RotationDirection;

public:
	FORCEINLINE float GetCurrentAcceleration() const { return CurrentAcceleration; }
	FORCEINLINE bool GetAccelerationReady() const { return bAccelerationReady; }

};
