// Fill out your copyright notice in the Description page of Project Settings.


#include "BaseCharacter.h"
#include "GameFramework/SpringArmComponent.h"
#include "Camera/CameraComponent.h"

// Sets default values
ABaseCharacter::ABaseCharacter()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	//GetMesh()->SetMorphTarget(FName("Trance Body"), 1.f);
	//GetMesh()->SetMorphTarget(FName("Trance HD"), 1.f);
	GetMesh()->SetMorphTarget(FName("Trance Head"), 1.f);
	GetMesh()->SetMorphTarget(FName("Trance Head HD Details"), 1.f);

	MeshBeard = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("MeshBeard"));
	MeshBeard->SetupAttachment(GetMesh());

	MeshBrows = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("MeshBrows"));
	MeshBrows->SetupAttachment(GetMesh());

	MeshShirt = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("MeshShirt"));
	MeshShirt->SetupAttachment(GetMesh());

	MeshShorts = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("MeshShorts"));
	MeshShorts->SetupAttachment(GetMesh());

	MeshShoes = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("MeshShoes"));
	MeshShoes->SetupAttachment(GetMesh());

	SpringArmComp = CreateDefaultSubobject<USpringArmComponent>(TEXT("SpringArmComp"));
	SpringArmComp->SetupAttachment(GetMesh());
	SpringArmComp->bUsePawnControlRotation = true;
	SpringArmComp->SetRelativeLocation(FVector(0, 0, 89.0f));

	CameraComp = CreateDefaultSubobject<UCameraComponent>(TEXT("CameraComp"));
	CameraComp->SetupAttachment(SpringArmComp);
}

// Called when the game starts or when spawned
void ABaseCharacter::BeginPlay()
{
	Super::BeginPlay();
	
}

void ABaseCharacter::MoveForward(float Value)
{
	AddMovementInput(GetActorForwardVector() * Value);
}

void ABaseCharacter::MoveRight(float Value)
{
	AddMovementInput(GetActorRightVector() * Value);
}

void ABaseCharacter::BeginCrouch()
{

}

void ABaseCharacter::EndCrouch()
{

}

// Called every frame
void ABaseCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void ABaseCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	PlayerInputComponent->BindAxis(TEXT("MoveForward"), this, &ABaseCharacter::MoveForward);
	PlayerInputComponent->BindAxis(TEXT("MoveRight"), this, &ABaseCharacter::MoveRight);

	PlayerInputComponent->BindAxis(TEXT("LookUp"), this, &ABaseCharacter::AddControllerPitchInput);
	PlayerInputComponent->BindAxis(TEXT("Turn"), this, &ABaseCharacter::AddControllerYawInput);

	PlayerInputComponent->BindAction(TEXT("Crouch"), IE_Pressed, this, &ABaseCharacter::BeginCrouch);
	PlayerInputComponent->BindAction(TEXT("Crouch"), IE_Released, this, &ABaseCharacter::EndCrouch);

	//PlayerInputComponent->BindAction(TEXT("Jump"), IE_Pressed, this, &ACharacter::Jump);
}

