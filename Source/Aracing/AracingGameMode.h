// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "AracingGameMode.generated.h"

UCLASS(minimalapi)
class AAracingGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AAracingGameMode();
};



