// Copyright Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class Aracing : ModuleRules
{
	public Aracing(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

		PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine", "InputCore", "HeadMountedDisplay" });
	}
}
