// Copyright Epic Games, Inc. All Rights Reserved.

#include "AracingGameMode.h"
#include "AracingCharacter.h"
#include "UObject/ConstructorHelpers.h"

AAracingGameMode::AAracingGameMode()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/zAssets/ThirdPersonCPP/Blueprints/ThirdPersonCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}
